import { useEffect, useContext } from "react";
import { Routes, Route, useNavigate } from "react-router-dom";
import "./App.css";
import Header from "./Components/Header/Header";
import HomePage from "./Components/HomePage/HomePage";
import ErrorPage from "./Components/ErrorPage/ErrorPage";
import BoardDetails from "./Components/BoardDetails/BoardDetails";
import { fetchData, deleteData, postData } from "./Components/API/api";
import { AlertContext } from "./Components/AlertContext/AlertContext";
import ErrorAlert from "./Components/ErrorAlert/ErrorAlert";
import Loader from "./Components/Loader/Loader";
import { useDispatch, useSelector } from "react-redux";
import {
  fetchBoards,
  postBoards,
  deleteBoards,
  toggleLoading,
} from "./Components/reduxStore/boardSlice";

function App() {
  const boards = useSelector((state) => state.boards.boards);
  const isLoading = useSelector((state) => state.boards.isLoading);

  const { handleOpen } = useContext(AlertContext);
  const navigate = useNavigate();
  const dispatch = useDispatch();

  useEffect(() => {
    fetchData("members/me/boards").then(({ data, result }) => {
      result
        ? dispatch(fetchBoards(data))
        : handleOpen(data || "Error while fetching data", "error");
    });
    dispatch(toggleLoading());
  }, []);

  const deleteBoardHandler = (id) => {
    deleteData(`boards/${id}`).then(({ data, result }) => {
      if (result === true) {
        dispatch(deleteBoards(id));
        navigate("/");
      } else {
        handleOpen(data || "Error while deleting data", "error");
      }
    });
  };

  const addBoardHandle = (inputVal) => {
    postData(`boards?name=${inputVal}`).then(({ data, result }) => {
      result
        ? dispatch(postBoards(data))
        : handleOpen(data || "Error while creating board", "error");
    });
  };

  return (
    <>
      <Header />
      <Routes>
        <Route
          path="/"
          element={
            isLoading ? (
              <Loader />
            ) : (
              <HomePage boards={boards} onAddBoardHandler={addBoardHandle} />
            )
          }
        />
        <Route
          path="/boards/:id"
          element={
            <BoardDetails
              boards={boards}
              onDeleteBoardHandler={deleteBoardHandler}
            />
          }
        />
        <Route path="*" element={<ErrorPage />} />
      </Routes>
      <ErrorAlert />
    </>
  );
}

export default App;
