import React, { useState } from "react";
import { Card, Button, Stack, TextField, ButtonGroup } from "@mui/material";


const CreateCheckList = ({ id, onAddCheckListHandler }) => {
  
  const [showAddCheckListField, setShowAddCheckListField] = useState(false);
  const [inputVal, setInputVal] = useState("");

  const submitHandler = (event) => {
    event.preventDefault();

    onAddCheckListHandler(inputVal, id);
    setShowAddCheckListField(false);
  };

  return (
    <Card sx={{ width: "280px", marginTop: "10px" }}>
      {!showAddCheckListField && (
        <Button
          size="medium"
          onClick={() => {
            setShowAddCheckListField(true);
          }}
        >
          <i
            className="fa-regular fa-square-check"
            style={{ marginRight: "5px", color: "blue" }}
          ></i>
          <span style={{ color: "blue" }}> Add Check List</span>
        </Button>
      )}
      {showAddCheckListField && (
        <form onSubmit={submitHandler} style={{ width: "100%" }}>
          <Stack direction="column" spacing={2} sx={{ width: "100%" }}>
            <TextField
              multiline={true}
              sx={{ width: "100%" }}
              required
              id="outlined-required"
              label="Title"
              variant="outlined"
              size="small"
              onChange={(e) => {
                setInputVal(e.target.value);
              }}
            />
            <ButtonGroup aria-label=" secondary button group">
              <Button
                type="submit"
                size="small"
                variant="contained"
                sx={{ marginRight: "10px" }}
              >
                Add Checklist
              </Button>
              <Button
                size="small"
                variant="text"
                onClick={() => {
                  setShowAddCheckListField(false);
                }}
              >
                X
              </Button>
            </ButtonGroup>
          </Stack>
        </form>
      )}
    </Card>
  );
};

export default CreateCheckList;
