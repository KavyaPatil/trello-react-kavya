import React from "react";
import { Button, AppBar, Toolbar, IconButton, Typography } from "@mui/material";
import { Link } from "react-router-dom";
import "./Header.css";

const Header = () => {
  return (
    <>
      <AppBar position="static">
        <Toolbar>
          <Link to={"/"}>
            <Button color="inherit">Boards</Button>
          </Link>
          <IconButton
            color="inherit"
            aria-label="menu"
            sx={{ marginLeft: "40%" }}
          >
            <i className="fa-brands fa-trello"></i>
          </IconButton>
          <Typography variant="h6" sx={{ flexGrow: 1 }}>
            Trello
          </Typography>
          {/* <Button color="inherit">SignUp/LogIn</Button> */}
        </Toolbar>
      </AppBar>
    </>
  );
};

export default Header;
