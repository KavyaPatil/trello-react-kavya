import axios from "axios";

const baseUrl = "https://api.trello.com/1";
const token =
  "ATTAf80d72b66042a0129699e36e1fbc5fbe0f088c0ce40b04581fd142c69f8d7ce932B2BF7D";
const key = "9247dc8f0574aea72d4d314690c9c529";

async function fetchData(endpoint) {
  try {
    const response = await axios.get(
      `${baseUrl}/${endpoint}?key=${key}&token=${token}`
    );
    return { data: response.data, result: true };
  } catch (error) {
    return { data: error.message, result: false };
  }
}

async function postData(endpoint) {
  try {
    const response = await axios.post(
      `${baseUrl}/${endpoint}&key=${key}&token=${token}`
    );
    return { data: response.data, result: true };
  } catch (error) {
    return { data: error.message, result: false };
  }
}

async function deleteData(endpoint) {
  try {
    const response = await axios.delete(
      `${baseUrl}/${endpoint}?key=${key}&token=${token}`
    );
    return { data: response.data, result: true };
  } catch (error) {
    return { data: error.message, result: false };
  }
}

async function putData(endpoint) {
  try {
    const response = await axios.put(
      `${baseUrl}/${endpoint}&key=${key}&token=${token}`
    );
    return { data: response.data, result: true };
  } catch (error) {
    return { data: error.message, result: false };
  }
}

export { fetchData, postData, deleteData, putData };
