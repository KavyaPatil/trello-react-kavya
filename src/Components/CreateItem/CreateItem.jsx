import React, { useState } from "react";
import { Card, Button, Stack, TextField, ButtonGroup } from "@mui/material";

const CreateItem = ({ id, onAddItemHandler }) => {
  const [showAddItemField, setShowAddItemField] = useState(false);
  const [inputVal, setInputVal] = useState("");

  const submitHandler = (event) => {
    event.preventDefault();
    setShowAddItemField(false);
    onAddItemHandler(inputVal, id);
  };

  return (
    <Card sx={{ width: "280px" }}>
      {!showAddItemField && (
        <Button
          size="medium"
          onClick={() => {
            setShowAddItemField(true);
          }}
        >
          add item
        </Button>
      )}
      {showAddItemField && (
        <form onSubmit={submitHandler} style={{ width: "100%" }}>
          <Stack direction="column" spacing={2} sx={{ width: "100%" }}>
            <TextField
              multiline={true}
              sx={{ width: "100%" }}
              required
              id="outlined-required"
              label="Title"
              variant="outlined"
              size="small"
              //   helperText={inputVal ? "" : "List name is required"}
              onChange={(e) => {
                setInputVal(e.target.value);
              }}
            />
            <ButtonGroup aria-label=" secondary button group">
              <Button
                type="submit"
                size="small"
                variant="contained"
                sx={{ marginRight: "10px" }}
              >
                Add item
              </Button>
              <Button
                size="small"
                variant="text"
                onClick={() => {
                  setShowAddItemField(false);
                }}
              >
                X
              </Button>
            </ButtonGroup>
          </Stack>
        </form>
      )}
    </Card>
  );
};

export default CreateItem;
