import { Card, Typography } from "@mui/material";
import React from "react";

const Boards = ({ item }) => {

  return (
    <Card
      sx={{
        height: "200px",
        backgroundColor: `${item.prefs.backgroundColor}`,
        textAlign: "center",
        backgroundImage: `url(${item.prefs.backgroundImage})`,
      }}
    >
      <Typography variant="h6" color={"white"} sx={{marginTop:'20%'}}>{item.name}</Typography>
    </Card>
  );
};

export default Boards;
