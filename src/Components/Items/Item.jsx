import React from "react";
import { ListItem, Checkbox, ListItemText, Button } from "@mui/material";

const Item = ({ item, cardId, onChangeState, onDeleteItem }) => {
  
  const handleCheckBox = (event) => {
    const checked = event.target.checked;
    const state = checked ? "complete" : "incomplete";
    onChangeState(item, cardId, state);
  };

  const deleteItemHandler = () => {
      onDeleteItem(item.idChecklist, item.id)
  }

  return (
    <ListItem>
      <Checkbox
        onChange={handleCheckBox}
        color="primary"
        inputProps={{ "aria-label": item.name }}
        checked={item.state === "complete" ? true : false}
      />
      <ListItemText primary={item.name} />
      <Button onClick={deleteItemHandler}>X</Button>
    </ListItem>
  );
};

export default Item;
