import React, {useContext } from 'react';
import Snackbar from '@mui/material/Snackbar';
import MuiAlert from '@mui/material/Alert';
import { AlertContext } from '../AlertContext/AlertContext';

const Alert = React.forwardRef(function Alert(props, ref) {
  return <MuiAlert elevation={6} ref={ref} variant="filled" {...props} />;
});

const ErrorAlert = () => {

    const { open, message, severity, handleClose } = useContext(AlertContext);
    
//   const handleClose = (event, reason) => {

//     if (reason === 'clickaway') {
//       return;
//     }
//     onClose();
//   };

  return (
    <Snackbar open={open} autoHideDuration={6000} onClose={handleClose}>
      <Alert onClose={handleClose} severity={severity}>
        {message}
      </Alert>
    </Snackbar>
  );
};

export default ErrorAlert;