
import  { createContext, useState } from "react";



export const AlertContext = createContext();

export const AlertProvider = ({ children }) => {
  const [open, setOpen] = useState(false);
  const [message, setMessage] = useState("");
  const [severity, setSeverity] = useState("");

  const handleClose = () => {
    setOpen(false);
    setMessage("");
  };

  const handleOpen = (newMessage, severity) => {
    setOpen(true);
    setMessage(newMessage);
    setSeverity(severity)
  };

  return (
    <AlertContext.Provider value={{ open, message, handleClose, handleOpen , severity}}>
      {children}
    </AlertContext.Provider>
  );
};
