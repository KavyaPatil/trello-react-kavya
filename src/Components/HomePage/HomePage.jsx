import React, { useState, useEffect } from "react";
import { Button, Container, Grid, Typography } from "@mui/material";
import CreateBoard from "../CreateBoard/CreateBoard";
import Boards from "../Boards/Boards";
import { Link } from "react-router-dom";
import Loader from "../Loader/Loader";

const HomePage = ({ boards, onAddBoardHandler}) => {
  const [showCreateBoard, setShowCreateBoard] = useState(false);


  return (
    <>
      <Container sx={{ width: "80%", margin: "auto", padding: "20px" }}>
        <Button
          color="inherit"
          onClick={() => {
            setShowCreateBoard(!showCreateBoard);
          }}
        >
          Create Board
        </Button>

        {showCreateBoard && (
          <CreateBoard
            addBoardHandler={onAddBoardHandler}
            onCreateBoard={() => setShowCreateBoard(!showCreateBoard)}
          />
        )}
        <hr />
        <Typography variant="h5" sx={{ flexGrow: 1, margin: "20px 0px" }}>
          All Boards
        </Typography>
        <Grid container spacing={2}>
          {boards.length ? (
            boards.map((item) => {
              return (
                <Grid item xs={12} sm={6} md={4} key={item.id}>
                  <Link to={`/boards/${item.id}`}>
                    <Boards key={item.id} item={item} />
                  </Link>
                </Grid>
              );
            })
          ) : (
            <Typography variant="h6" sx={{ flexGrow: 1, margin: "20px 0px" }}>
              No Boards are Created...
            </Typography>
          )}
        </Grid>
      </Container>
    </>
  );
};

export default HomePage;
