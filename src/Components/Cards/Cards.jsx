import React, { useState } from "react";
import { Button, Card, Stack, Tooltip, Typography } from "@mui/material";
import CardDetails from "../CardDetails/CardDetails";

const Cards = ({ card, onDeleteCardHandler }) => {
  
  const [showCardDetails, setShowCardDetails] = useState(false); 

  const deletCardHandler = (event) => {
    event.stopPropagation()
    onDeleteCardHandler(card);
  };

  const HandleModalOpen = () => {
    console.log('card opened')
    setShowCardDetails(true);
  };

  const HandleModalClose = () => {
    setShowCardDetails(false);
  };

  return (
    <>
      <Card
        sx={{ padding: "10px", minWidth: "280px" }}
        onClick={HandleModalOpen}
      >
        <Stack direction="row" spacing="auto">
          <Typography variant="p">{card.name}</Typography>
          <Tooltip title="Delete card">
            <Button onClick={deletCardHandler}>X</Button>
          </Tooltip>
        </Stack>
      </Card>

      <CardDetails
        card={card}
        show={showCardDetails}
        onClose={HandleModalClose}
      />
    </>
  );
};

export default Cards;
