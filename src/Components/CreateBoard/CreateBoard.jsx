import { Stack, TextField, Button } from "@mui/material";
import React, { useState } from "react";
import "./CreateBoard.css";


const CreateBoard = ({ addBoardHandler, onCreateBoard }) => {
  const [inputVal, setInputVal] = useState("");


  const submitHandler = (event) => {
    
    event.preventDefault();
    addBoardHandler(inputVal);
        onCreateBoard();
    
  };

  return (
    <form onSubmit={submitHandler}>
      <Stack spacing={4} sx={{ padding: "10px" }} direction={"column"}>
        <TextField
          required
          id="outlined-required"
          label="Board Title"
          variant="outlined"
          helperText={inputVal ? "" : "Board title is required"}
          onChange={(e) => {
            setInputVal(e.target.value);
          }}
        />
        <Button variant="contained" color="primary" type="submit">
          Create
        </Button>{" "}
      </Stack>
    </form>
  );
};

export default CreateBoard;
