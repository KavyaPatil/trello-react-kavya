import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  boards: [],
  isLoading: true,
};

const boardsSlice = createSlice({
  name: "boards",
  initialState,
  reducers: {
    fetchBoards: (state, action) => {
      state.boards = action.payload;
    },
    postBoards: (state, action) => {
      state.boards.push(action.payload);
    },
    deleteBoards: (state, action) => {
      state.boards = state.boards.filter((item) => item.id !== action.payload);
    },
    toggleLoading: (state, action) => {
      state.isLoading = false;
    },
  },
});

export const { fetchBoards, postBoards, deleteBoards, toggleLoading } =
  boardsSlice.actions;

export default boardsSlice.reducer;
