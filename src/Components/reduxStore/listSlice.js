import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  lists: [],
};

const listsSlice = createSlice({
  name: "lists",
  initialState,
  reducers: {
    fetchLists: (state, action) => {
      state.lists = action.payload;
    },
    postLists: (state, action) => {
      state.lists.push(action.payload);
    },
    deleteLists: (state, action) => {
      state.lists = state.lists.filter((item) => item.id !== action.payload);
    },
    putLists: (state, action) => {
      state.lists = state.lists.map((item) =>
        item.id === action.payload.id ? action.payload.data : item
      );
    },
  },
});

export const { fetchLists, postLists, deleteLists, putLists } =
  listsSlice.actions;

export default listsSlice.reducer;
