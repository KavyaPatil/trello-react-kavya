import { configureStore } from '@reduxjs/toolkit';
import boardsReducer from './boardSlice';
import listsReducer from './listSlice';
import cardsReducer from './cardSlice';
import checklistsReducer from './ChecklistSlice';
import itemsReducer from './itemSlice';



const store = configureStore({
  reducer: {
    boards: boardsReducer,
    lists: listsReducer,
    cards: cardsReducer,
    checklists: checklistsReducer,
    items : itemsReducer
  }
});

export default store;
