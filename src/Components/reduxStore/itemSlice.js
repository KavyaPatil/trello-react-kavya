import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  items: {},
};

const itemsSlice = createSlice({
  name: "items",
  initialState,
  reducers: {
    fetchItems: (state, action) => { 
      // console.log(action.payload)
     state.items[action.payload.idChecklist] = action.payload.data;
    },
    postItems: (state, action) => {
      // console.log(action.payload)
      const idChecklist = action.payload.idChecklist;
      if(state.items[idChecklist]){
        state.items[idChecklist] = [ ...state.items[action.payload.idChecklist],action.payload.item]
      } else {
        state.items[idChecklist] = [action.payload.item];
      }
    },
    deleteItems: (state, action) => {
      
      const idChecklist = action.payload.idChecklist;
      state.items[idChecklist] = state.items[idChecklist].filter(
        (item) => item.id !== action.payload.id
      );
    },
    putItems: (state, action) => {
      // console.log(action.payload)
      const idChecklist = action.payload.idChecklist;
      state.items[idChecklist] = state.items[idChecklist].map((item) =>
      item.id === action.payload.id ? action.payload.item : item
      );
    },
  },
});

export const { fetchItems, postItems, deleteItems, putItems} =
  itemsSlice.actions;

export default itemsSlice.reducer;
