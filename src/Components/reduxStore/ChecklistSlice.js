import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  checklists: {},
  isLoading: true,
};

const checklistsSlice = createSlice({
  name: "checklists",
  initialState,
  reducers: {
    fetchChecklists: (state, action) => {
      state.checklists[action.payload.idCard] = action.payload.data;
    },
    postChecklists: (state, action) => {
      const idCard = action.payload.idCard;
      if(state.checklists[idCard]){
        state.checklists[idCard] = [ ...state.checklists[action.payload.idCard],action.payload.checklist]
      } else {
        state.checklists[idCard] = [action.payload.checklist];
      }
    },
    deleteChecklists: (state, action) => {
      const idCard = action.payload.idCard;
      state.checklists[idCard] = state.checklists[idCard].filter(
        (item) => item.id !== action.payload.id
      );
    },
    toggleLoading: (state, action) => {
      state.isLoading = false;
    },
  },
});

export const {
  fetchChecklists,
  postChecklists,
  deleteChecklists,
  toggleLoading,
} = checklistsSlice.actions;

export default checklistsSlice.reducer;
