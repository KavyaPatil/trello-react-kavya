import { createSlice } from "@reduxjs/toolkit";

const initialState = { cards: {} };

const cardSlice = createSlice({
  name: "cards",
  initialState,
  reducers: {
    fetchCards: (state, action) => {
      state.cards[action.payload.idList] = action.payload.data;
    },
    postCards: (state, action) => {
      const idList = action.payload.idList;
      if(state.cards[idList]){
        state.cards[idList] = [ ...state.cards[action.payload.idList],action.payload.card]
      } else {
        state.cards[idList] = [action.payload.card];
      }
    },
    deleteCards: (state, action) => {
      //   state.cards = state.cards.filter((card) => item.id !== action.payload);
      const idList = action.payload.idList;
      state.cards[idList] = state.cards[idList].filter(
        (item) => item.id !== action.payload.id
      );
    },
  },
});

export const { fetchCards, postCards, deleteCards } = cardSlice.actions;

export default cardSlice.reducer;
