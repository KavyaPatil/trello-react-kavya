import React from 'react';
import "./InternalServer.css";
import { useNavigate } from 'react-router-dom';


const InternalServer = () => {

    const navigate = useNavigate();

  return (
    <div className="error-container">
    {/* <p className="error-code">Error 500</p> */}
    <p className="error-message">Oops! Something went wrong.</p>
    <p>Please try again later.</p>
    <button className="error-retry-button" onClick={()=>navigate('/')}>
      Retry
    </button>
  </div>
  )
}

export default InternalServer;