import { useContext, useEffect, useState, useReducer } from "react";
import { useParams } from "react-router-dom";
import { Container, Stack, Button, Typography, Tooltip } from "@mui/material";
import List from "../Lists/List";
import CreateList from "../CreateList/CreateList";
import Loader from "../Loader/Loader";
import "./BoardDetails.css";
import { fetchData, postData, putData } from "../API/api";
import { AlertContext } from "../AlertContext/AlertContext";
import ErrorAlert from "../ErrorAlert/ErrorAlert";
import { postLists, fetchLists, deleteLists } from "../reduxStore/listSlice";
import { useDispatch, useSelector } from "react-redux";

const BoardDetails = ({ boards, onDeleteBoardHandler }) => {
  
  const lists = useSelector((state) => state.lists.lists);
  const dispatch = useDispatch();
  const [isLoading, setIsLoading] = useState(true);

  const { handleOpen } = useContext(AlertContext);

  const { id } = useParams();

  const selectedBoard = boards.find((item) => {
    return id === item.id;
  });

  useEffect(() => {
    fetchData(`boards/${id}/lists`).then(({ data, result }) => {
      result
        ? dispatch(fetchLists(data))
        : handleOpen(data || "Error while fetching data", "error");
    });
    setIsLoading(false);
  }, []);

  const addListHandler = (inputVal, id) => {
    postData(`boards/${id}/lists?name=${inputVal}`).then(({ data, result }) => {
      console.log(data);
      result
        ? dispatch(postLists(data))
        : handleOpen(err.message || "Error while adding list", "error");
    });
  };

  const archiveListHandler = (item) => {
    putData(`lists/${item.id}?closed=true`).then(({ data, result }) => {
      result
        ? dispatch(deleteLists(item.id))
        : handleOpen(data || "Error while archiving list", "error");
    });
  };

  if (isLoading) {
    return <Loader />;
  }

  return (
    <>
      <Container
        sx={{ overflowX: "auto", minWidth: "100%", minHeight: "100vh" }}
      >
        <Stack direction={"row"}>
          <Typography variant="h4" color={"black"} sx={{ padding: "20px" }}>
            {selectedBoard ? selectedBoard.name : ""}{" "}
          </Typography>
          <Tooltip title="Delete Board">
            <Button
              onClick={() => onDeleteBoardHandler(id)}
              sx={{
                textTransform: "capitalize",
                textDecorationLine: "underline",
              }}
            >
              delete
            </Button>
          </Tooltip>
        </Stack>
        <Stack
          spacing={2}
          direction="row"
          sx={{ margin: "auto", padding: "10px" }}
        >
          {selectedBoard && lists.length
            ? lists.map((item) => {
                return (
                  <List
                    key={item.id}
                    item={item}
                    onArchiveHandler={archiveListHandler}
                  />
                );
              })
            : null}
          <CreateList id={id} onAddListHandler={addListHandler} />
        </Stack>
      </Container>
      <ErrorAlert />
    </>
  );
};

export default BoardDetails;
