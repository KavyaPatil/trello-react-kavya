import React, { useState } from "react";
import { Card, Button, Stack, TextField, ButtonGroup } from "@mui/material";


const CreateList = ({ id, onAddListHandler }) => {
  const [showAddListField, setShowAddListField] = useState(false);
  const [inputVal, setInputVal] = useState("");


  const submitHandler = (event) => {
    event.preventDefault();

    onAddListHandler(inputVal, id);
    setShowAddListField(false);
  };

  return (
    <div>
      <Card sx={{ minWidth: "300px" }}>
        {!showAddListField && (
          <Button
            size="large"
            onClick={() => {
              setShowAddListField(true);
            }}
            sx={{ color: "blue" }}
          >
            + Create a new list
          </Button>
        )}
        {showAddListField && (
          <form onSubmit={submitHandler} style={{ minWidth: "100%" }}>
            <Stack direction="column" spacing={2} sx={{ width: "100%" }}>
              <TextField
                sx={{ width: "100%" }}
                required
                id="outlined-required"
                label="List Title"
                variant="outlined"
                onChange={(e) => {
                  setInputVal(e.target.value);
                }}
              />
              <ButtonGroup aria-label=" secondary button group">
                <Button
                  type="submit"
                  size="large"
                  variant="contained"
                  sx={{ marginRight: "10px" }}
                >
                  Add list
                </Button>
                <Button
                  size="small"
                  variant="text"
                  onClick={() => {
                    setShowAddListField(false);
                  }}
                >
                  X
                </Button>
              </ButtonGroup>
            </Stack>
          </form>
        )}
      </Card>
    </div>
  );
};

export default CreateList;
