export default function reducer(state, action) {
  switch (action.type) {
    case "FETCH":
      return { data: action.payload };

    case "POST":
      return { data: [...state.data, action.payload] };

    case "DELETE":
      const updatedState = state.data.filter((item) => {
        return item.id !== action.payload;
      });
      return { data: updatedState };

    case "PUT":
      const updated = state.data.map((item) => {
        if (item.id === action.payload.id) {
          return action.payload.data;
        } else {
          return item;
        }
      });
      return { data: updated };

    default:
      return state;
  }
}
