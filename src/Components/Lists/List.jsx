import {
  CardContent,
  Typography,
  Stack,
  Button,
  Card,
  Tooltip,
} from "@mui/material";
import React, { useEffect, useContext, useReducer } from "react";
import CreateCard from "../CreateCard/CreateCard";
import Cards from "../Cards/Cards";
import { deleteData, fetchData, postData } from "../API/api";
import { AlertContext } from "../AlertContext/AlertContext";
import ErrorAlert from "../ErrorAlert/ErrorAlert";
import { useDispatch, useSelector } from "react-redux";
import { deleteCards, fetchCards, postCards } from "../reduxStore/cardSlice";

const List = ({ item, onArchiveHandler }) => {

  const cards = useSelector((state) => state.cards.cards[item.id])
  // console.log(cards)
  // console.log(cards)
  const dispatch = useDispatch();

  const { handleOpen } = useContext(AlertContext);

  useEffect(() => {
    fetchData(`lists/${item.id}/cards`).then(({ data, result }) => {
      // console.log(fetchCards)
      result
        ? dispatch(fetchCards({idList:item.id,data:data}))
        : handleOpen(data || "Error while fetching cards", "error");
    });
  }, []);

  const addCardHandler = (inputVal, id) => {
    console.log(id, 'cardID');
    postData(`lists/${id}/cards?name=${inputVal}`).then(({ data, result }) => {
      console.log(data)
      result
        ? dispatch(postCards({idList:item.id, card:data}))
        : handleOpen(data || "Error while adding cards", "error");
    });
  };

  const deleteCardHandler = (card) => {
    deleteData(`cards/${card.id}`).then(({ data, result }) => {
      result
        ? dispatch(deleteCards({idList:item.id,id:card.id}))
        : handleOpen(data || "Error while deleting cards", "error");
    });
  };

  return (
    <div>
      <Card sx={{ minWidth: "300px" }}>
        <CardContent>
          <Stack direction="row" spacing="auto" sx={{ minWidth: "100%" }}>
            <Typography variant="h5" component="div">
              {item.name}
            </Typography>
            <Tooltip title="Archive List">
              <Button onClick={() => onArchiveHandler(item)}>
                <i className="fa-solid fa-box-archive"></i>
              </Button>
            </Tooltip>
          </Stack>
          <Stack direction="column" spacing={2}>
            {cards
              ? cards.map((card) => {
                  return (
                    <Cards
                      key={card.id}
                      card={card}
                      onDeleteCardHandler={deleteCardHandler}
                    />
                  );
                })
              : null}
            <CreateCard id={item.id} onAddCardHandler={addCardHandler} />
          </Stack>
        </CardContent>
      </Card>
      <ErrorAlert />
    </div>
  );
};

export default List;
