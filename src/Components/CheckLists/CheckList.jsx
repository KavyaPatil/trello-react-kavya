import { useEffect, useContext } from "react";
import { Card, Stack, Typography, Tooltip, Button, List } from "@mui/material";
import ProgressBar from "./ProgressBar";
import CreateItem from "../CreateItem/CreateItem";
import Item from "../Items/Item";
import { deleteData, fetchData, postData, putData } from "../API/api";
import { AlertContext } from "../AlertContext/AlertContext";
import ErrorAlert from "../ErrorAlert/ErrorAlert";
import { useDispatch, useSelector } from "react-redux";
import {
  fetchItems,
  postItems,
  deleteItems,
  putItems,
} from "../reduxStore/itemSlice";

const CheckList = ({ checklist, ondeleteChecklist }) => {

  const items = useSelector((state) => state.items.items[checklist.id])
  // console.log(items, 'itemss corrected')
  const dispatch = useDispatch();
  const { handleOpen } = useContext(AlertContext);

  useEffect(() => {
    fetchData(`checklists/${checklist.id}/checkItems`).then(
      ({ data, result }) => {
        // console.log(data)
        result
          ? dispatch(fetchItems({idChecklist:checklist.id, data:data}))
          : handleOpen(data || "Error while fetching items", "error");
      }
    );
  }, [checklist.id]);

  const completedItems = items ? items.filter((item) => item.state === "complete"):0;
  const progress = items ? (completedItems.length / items.length) * 100 : 0;

  const changeStateHandler = (item, cardId, state) => {
    // console.log(item.id,'CheckItemId')
    putData(`cards/${cardId}/checkItem/${item.id}?state=${state}`).then(
      ({ data, result }) => {
        result
          ? dispatch(putItems({ id: item.id, item: data, idChecklist:checklist.id }))
          : handleOpen(data || "Error while updating items", "error");
      }
    );
  };

  const deleteCheckList = () => {
    ondeleteChecklist(checklist.id);
  };

  const addItemHandler = (inputVal, id) => {
    postData(`checklists/${id}/checkItems?name=${inputVal}`).then(
      ({ data, result }) => {
        result
          ? dispatch(postItems({item: data, idChecklist:id }))
          : handleOpen(data || "Error while adding item", "error");
      }
    );
  };

  const deleteItemHandler = (checklistId, id) => {
    deleteData(`checklists/${checklistId}/checkItems/${id}`).then(
      ({ data, result }) => {
        result
          ? dispatch(deleteItems({id: id, idChecklist:checklist.id }))
          : handleOpen(data || "Error while deleting item", "error");
      }
    );
  };

  return (
    <>
      <Card
        sx={{
          padding: "10px",
          minWidth: "280px",
          boxShadow: "2px 2px 4px rgba(0, 0, 0, 0.3)",
        }}
      >
        <Stack direction="row" spacing="auto">
          <div>
            <i
              className="fa-regular fa-square-check"
              style={{ marginRight: "10px" }}
            ></i>
            <Typography variant="span">{checklist.name}</Typography>
          </div>
          <Tooltip title="Delete checklist">
            <Button onClick={deleteCheckList}>X</Button>
          </Tooltip>
        </Stack>
        <span>{items ? Math.floor(progress) : 0}%</span>
        <span>
          <ProgressBar value={items ? progress : 0} />
        </span>
        <List>
          {items
            ? items.map((item) => {
                return (
                  <Item
                    item={item}
                    key={item.id}
                    cardId={checklist.idCard}
                    onChangeState={changeStateHandler}
                    onDeleteItem={deleteItemHandler}
                  />
                );
              })
            : null}
        </List>
        <CreateItem id={checklist.id} onAddItemHandler={addItemHandler} />
      </Card>
      <ErrorAlert />
    </>
  );
};

export default CheckList;
