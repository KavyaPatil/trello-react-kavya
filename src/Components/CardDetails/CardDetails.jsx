import {
  Dialog,
  DialogContent,
  Button,
  Stack,
  Typography,
} from "@mui/material";
import React, { useContext, useEffect, useState } from "react";
import CreateCheckList from "../CreateCheckList/CreateCheckList";
import CheckList from "../CheckLists/CheckList";
import Loader from "../Loader/Loader";
import { fetchData, deleteData, postData } from "../API/api";
import { AlertContext } from "../AlertContext/AlertContext";
import ErrorAlert from "../ErrorAlert/ErrorAlert";
import { postChecklists, fetchChecklists, deleteChecklists } from "../reduxStore/ChecklistSlice";
import { useDispatch, useSelector } from "react-redux";

const CardDetails = ({ card, show, onClose }) => {

  const checkLists = useSelector((state) => state.checklists.checklists[card.id]);

  const dispatch = useDispatch();
  const [isLoading, setIsLoading] = useState(true);
  const { handleOpen } = useContext(AlertContext);

  useEffect(() => {
    fetchData(`cards/${card.id}/checklists`).then(({ data, result }) => {
      // console.log(data)
      result
        ? dispatch(fetchChecklists({data:data, idCard:card.id}))
        : handleOpen(data || "Error while fetching checklists", "error");
    });
    setIsLoading(false);
  }, []);

  const addCheckListHandler = (inputVal, id) => {
    postData(`cards/${id}/checklists?name=${inputVal}`).then(
      ({ data, result }) => {
        result
          ? dispatch(postChecklists({checklist:data, idCard:card.id}))
          : handleOpen(data || "Error while adding checklist", "error");
      }
    );
  };

  const deleteCheckListHandler = (checklistId) => {
    deleteData(`checklists/${checklistId}`).then(({ data, result }) => {
      result
        ? dispatch(deleteChecklists({checklist:data, idCard:card.id}))
        : handleOpen(data || "Error while deleting checklist", "error");
    });
  };

  if (isLoading) {
    return <Loader />;
  }

  return (
    <>
      <Dialog open={show} onClose={onClose}>
        <div style={{ padding: "10px", minHeight: "600px" }}>
          <DialogContent>
            <Stack direction={"row"} spacing={50}>
              <Typography variant="h5" sx={{ paddingBottom: "20px" }}>
                {card.name}
              </Typography>
              <Button onClick={onClose}>X</Button>
            </Stack>
            <Stack direction={"column"} spacing={2}>
              {checkLists
                ? checkLists.map((item) => {
                    return (
                      <CheckList
                        checklist={item}
                        key={item.id}
                        ondeleteChecklist={deleteCheckListHandler}
                      />
                    );
                  })
                : null}
            </Stack>
            <CreateCheckList
              id={card.id}
              onAddCheckListHandler={addCheckListHandler}
            />
          </DialogContent>
        </div>
      </Dialog>
      <ErrorAlert />
    </>
  );
};

export default CardDetails;
