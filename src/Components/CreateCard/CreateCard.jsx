import React, { useState } from "react";
import { Card, Button, Stack, TextField, ButtonGroup } from "@mui/material";


const CreateCard = ({ id, onAddCardHandler }) => {
  const [showAddCardField, setShowAddCardField] = useState(false);
  const [inputVal, setInputVal] = useState("");

  const submitHandler = (event) => {
    event.preventDefault();

    onAddCardHandler(inputVal, id);
    setShowAddCardField(false);
  };

  return (
    <Card sx={{ width: "280px" }}>
      {!showAddCardField && (
        <Button
          size="medium"
          onClick={() => {
            setShowAddCardField(true);
          }}
        >
          + Add Card
        </Button>
      )}
      {showAddCardField && (
        <form onSubmit={submitHandler} style={{ width: "100%" }}>
          <Stack direction="column" spacing={2} sx={{ width: "100%" }}>
            <TextField
              multiline={true}
              sx={{ width: "100%" }}
              required
              id="outlined-required"
              label="Card Title"
              variant="outlined"
              size="small"
              //   helperText={inputVal ? "" : "List name is required"}
              onChange={(e) => {
                setInputVal(e.target.value);
              }}
            />
            <ButtonGroup aria-label=" secondary button group">
              <Button
                type="submit"
                size="small"
                variant="contained"
                sx={{ marginRight: "10px" }}
              >
                Add list
              </Button>
              <Button
                size="small"
                variant="text"
                onClick={() => {
                  setShowAddCardField(false);
                }}
              >
                X
              </Button>
            </ButtonGroup>
          </Stack>
        </form>
      )}
    </Card>
  );
};

export default CreateCard;
